// vite.config.ts
import { resolve, dirname } from "node:path";
import { fileURLToPath } from "node:url";
import { defineConfig } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/vite@4.0.4_@types+node@18.11.18_sass@1.57.1/node_modules/vite/dist/node/index.js";
import Vue from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/@vitejs+plugin-vue@4.0.0_vite@4.0.4_vue@3.2.45/node_modules/@vitejs/plugin-vue/dist/index.mjs";
import VueRouter from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/unplugin-vue-router@0.3.0_rollup@3.9.1_vue-router@4.1.6_vue@3.2.45/node_modules/unplugin-vue-router/dist/vite.mjs";
import { VueRouterAutoImports } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/unplugin-vue-router@0.3.0_rollup@3.9.1_vue-router@4.1.6_vue@3.2.45/node_modules/unplugin-vue-router/dist/index.mjs";
import Components from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/unplugin-vue-components@0.22.12_rollup@3.9.1_vue@3.2.45/node_modules/unplugin-vue-components/dist/vite.mjs";
import AutoImport from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/unplugin-auto-import@0.12.1_@vueuse+core@9.10.0_rollup@3.9.1/node_modules/unplugin-auto-import/dist/vite.js";
import { VitePluginFonts } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/vite-plugin-fonts@0.7.0_vite@4.0.4/node_modules/vite-plugin-fonts/dist/index.js";
import { VitePluginRadar } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/vite-plugin-radar@0.6.0_vite@4.0.4/node_modules/vite-plugin-radar/dist/index.js";
import PurgeIcons from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/vite-plugin-purge-icons@0.9.2_vite@4.0.4/node_modules/vite-plugin-purge-icons/dist/index.mjs";
import ImageMin from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/vite-plugin-imagemin@0.6.1_vite@4.0.4/node_modules/vite-plugin-imagemin/dist/index.mjs";
import VueI18nPlugin from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/@intlify+unplugin-vue-i18n@0.8.1_vue-i18n@9.3.0-beta.12/node_modules/@intlify/unplugin-vue-i18n/lib/vite.mjs";
import { VitePWA } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/vite-plugin-pwa@0.14.1_vite@4.0.4_workbox-build@6.5.4_workbox-window@6.5.4/node_modules/vite-plugin-pwa/dist/index.mjs";
import purgecss from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/rollup-plugin-purgecss@5.0.0/node_modules/rollup-plugin-purgecss/lib/rollup-plugin-purgecss.js";

// vite-plugin-vuero-doc/index.ts
import { join, basename } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/pathe@1.0.0/node_modules/pathe/dist/index.mjs";
import { compileTemplate, parse } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/@vue+compiler-sfc@3.2.45/node_modules/@vue/compiler-sfc/dist/compiler-sfc.cjs.js";

// vite-plugin-vuero-doc/markdown.ts
import yaml from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/js-yaml@4.1.0/node_modules/js-yaml/dist/js-yaml.mjs";
import remarkShiki from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/@stefanprobst+remark-shiki@2.2.0/node_modules/@stefanprobst/remark-shiki/src/index.js";
import rehypeExternalLinks from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/rehype-external-links@2.0.1/node_modules/rehype-external-links/index.js";
import rehypeRaw from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/rehype-raw@6.1.1/node_modules/rehype-raw/index.js";
import rehypeSlug from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/rehype-slug@5.1.0/node_modules/rehype-slug/index.js";
import rehypeAutolinkHeadings from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/rehype-autolink-headings@6.1.1/node_modules/rehype-autolink-headings/index.js";
import rehypeStringify from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/rehype-stringify@9.0.3/node_modules/rehype-stringify/index.js";
import remarkParse from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/remark-parse@10.0.1/node_modules/remark-parse/index.js";
import remarkGfm from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/remark-gfm@3.0.1/node_modules/remark-gfm/index.js";
import remarkRehype from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/remark-rehype@10.1.0/node_modules/remark-rehype/index.js";
import remarkFrontmatter from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/remark-frontmatter@4.0.1/node_modules/remark-frontmatter/index.js";
import { getHighlighter } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/shiki-es@0.1.2/node_modules/shiki-es/dist/shiki.node.mjs";
import { unified } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/unified@10.1.2/node_modules/unified/index.js";
var langs = ["vue", "vue-html", "typescript", "bash", "scss"];
async function createProcessor(theme) {
  const highlighter = await getHighlighter({
    theme,
    langs
  });
  return unified().use(remarkParse).use(remarkFrontmatter).use(() => (tree, file) => {
    if (tree.children[0].type === "yaml") {
      file.data.frontmatter = yaml.load(tree.children[0].value);
    }
  }).use(remarkGfm).use(remarkShiki, { highlighter }).use(remarkRehype, { allowDangerousHtml: true }).use(rehypeRaw).use(rehypeExternalLinks, { rel: ["nofollow"], target: "_blank" }).use(rehypeSlug).use(rehypeAutolinkHeadings, {
    behavior: "append",
    content: {
      type: "element",
      tagName: "i",
      properties: {
        className: ["iconify toc-link-anchor"],
        dataIcon: "feather:link"
      },
      children: []
    }
  }).use(rehypeStringify);
}
async function createProcessors(theme) {
  return {
    light: await createProcessor(typeof theme === "string" ? theme : theme.light),
    dark: await createProcessor(typeof theme === "string" ? theme : theme.dark)
  };
}

// vite-plugin-vuero-doc/transform.ts
import { kebabCase } from "file:///D:/WebstormProjects/templateVuero/node_modules/.pnpm/scule@1.0.0/node_modules/scule/dist/index.mjs";
var SELF_CLOSING_TAG_REGEX = /<([^\s></]+)([^>]+)\/>/g;
var OPEN_TAG_REGEX = /<([^\s></]+)/g;
var CLOSE_TAG_REGEX = /<\/([^\s></]+)/g;
function transformExampleMarkup(raw) {
  let output = raw;
  let content = null;
  if (content = raw.match(/<!--example-->([\s\S]*?)<!--\/example-->/)) {
    const kebabContent = content[1].replaceAll(SELF_CLOSING_TAG_REGEX, (substring, tag) => {
      return substring.replace("/>", `></${tag.trim()}>`);
    }).replaceAll(OPEN_TAG_REGEX, (substring) => {
      return `<${kebabCase(substring.substring(1).trim())}`;
    }).replaceAll(CLOSE_TAG_REGEX, (substring) => {
      return `</${kebabCase(substring.substring(2).trim())}`;
    }).replaceAll("&#x27;", "'");
    output = output.replace(content[1], kebabContent);
  }
  return output;
}
function transformSlots(source, condition = "") {
  if (source.includes("<!--code-->") && source.includes("<!--example-->")) {
    return `<template ${condition} #default>${source}`.replace(
      `<!--code-->`,
      `</template><template ${condition} #code>
<slot name="code"><div v-pre>`
    ).replace(`<!--/code-->`, `</div></slot>
</template>`).replace(
      `<!--example-->`,
      `<template ${condition} #example>
<slot name="example">`
    ).replace(`<!--/example-->`, `</slot>
</template>`);
  }
  if (source.includes("<!--code-->")) {
    return `<template ${condition} #default>${source}`.replace(
      `<!--code-->`,
      `</template><template ${condition} #code>
<slot name="code"><div v-pre>`
    ).replace(
      `<!--/code-->`,
      `</div></slot>
</template>
<template ${condition} #example><slot name="example"></slot></template>`
    );
  }
  if (source.includes("<!--example-->")) {
    return `<template ${condition} #default>${source}`.replace(
      `<!--example-->`,
      `</template><template ${condition} #example>
<slot name="example">`
    ).replace(
      `<!--/example-->`,
      `</slot>
</template>
<template ${condition} #code><slot name="code"></slot></template>`
    );
  }
  return `<template ${condition} #default>${source}</template><template ${condition} #example><slot name="example"></slot></template><template ${condition} #code><slot name="code"></slot></template>`;
}

// vite-plugin-vuero-doc/index.ts
function parseId(id) {
  const index = id.indexOf("?");
  if (index < 0)
    return id;
  else
    return id.slice(0, index);
}
function VitePluginVueroDoc(options) {
  let config;
  let processors;
  const cwd = process.cwd();
  const pathPrefix = options.pathPrefix ? join(cwd, options.pathPrefix) : cwd;
  async function markdownToVue(id, raw) {
    var _a, _b;
    const path = parseId(id);
    const input = transformExampleMarkup(raw);
    if (!processors)
      processors = await createProcessors(options.shiki.theme);
    const [vFileLight, vFileDark] = await Promise.all([
      processors.light.process(input),
      processors.dark.process(input)
    ]);
    const contentLight = vFileLight.toString();
    const contentDark = vFileDark.toString();
    const frontmatter = ((_a = vFileLight.data) == null ? void 0 : _a.frontmatter) ?? {};
    const slotLight = transformSlots(contentLight, 'v-if="!darkmode.isDark"');
    const slotDark = transformSlots(contentDark, 'v-if="darkmode.isDark"');
    const sfc = [
      `<template>`,
      `  <${options.wrapperComponent} :frontmatter="frontmatter" :source-meta="sourceMeta">`,
      `    ${slotLight}`,
      `    ${slotDark}`,
      `  </${options.wrapperComponent}>`,
      `</template>`
    ].join("\n");
    const result = parse(sfc, {
      filename: path,
      sourceMap: true
    });
    if (result.errors.length || result.descriptor.template === null) {
      console.error(result.errors);
      throw new Error(`Markdown: unable to parse virtual file generated for "${id}"`);
    }
    const { code, errors } = compileTemplate({
      filename: path,
      id: path,
      source: result.descriptor.template.content,
      preprocessLang: result.descriptor.template.lang,
      transformAssetUrls: false
    });
    if (errors.length) {
      console.error(errors);
      throw new Error(`Markdown: unable to compile virtual file "${id}"`);
    }
    let sourceMeta = "undefined";
    if ((_b = options.sourceMeta) == null ? void 0 : _b.enabled) {
      sourceMeta = JSON.stringify({
        relativePath: path.substring(cwd.length),
        basename: basename(path),
        path: (config == null ? void 0 : config.isProduction) ? "" : path,
        editProtocol: (config == null ? void 0 : config.isProduction) ? "" : options.sourceMeta.editProtocol
      });
    }
    const output = [
      `import { reactive } from 'vue'`,
      `import { useDarkmode } from '/@src/stores/darkmode'`,
      code.replace("export function render", "function render"),
      `const __frontmatter = ${JSON.stringify(frontmatter)};`,
      `const setup = () => ({`,
      `  frontmatter: reactive(__frontmatter),`,
      `  darkmode: useDarkmode(),`,
      `  sourceMeta: ${sourceMeta},`,
      `});`,
      `const __script = { render, setup };`,
      (config == null ? void 0 : config.isProduction) ? "" : `__script.__hmrId = ${JSON.stringify(path)};`,
      `export default __script;`
    ].join("\n");
    return output;
  }
  return {
    name: "vite-plugin-vuero-doc",
    enforce: "pre",
    configResolved(_config) {
      config = _config;
    },
    async transform(raw, id) {
      if (id.endsWith(".md") && id.startsWith(pathPrefix)) {
        return await markdownToVue(id, raw);
      }
    }
  };
}

// vite.config.ts
var __vite_injected_original_import_meta_url = "file:///D:/WebstormProjects/templateVuero/vite.config.ts";
var MINIFY_IMAGES = process.env.MINIFY ? process.env.MINIFY === "true" : false;
var vite_config_default = defineConfig({
  root: process.cwd(),
  base: "/",
  publicDir: "public",
  logLevel: "info",
  server: {
    port: 3e3
  },
  optimizeDeps: {
    include: [
      "@ckeditor/ckeditor5-vue",
      "@ckeditor/ckeditor5-build-classic",
      "@iconify/iconify",
      "@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.min.js",
      "@vee-validate/zod",
      "@vueuse/core",
      "@vueuse/head",
      "@vueform/multiselect",
      "@vueform/slider",
      "axios",
      "billboard.js",
      "dayjs",
      "dropzone",
      "dragula",
      "defu",
      "filepond",
      "filepond-plugin-file-validate-size",
      "filepond-plugin-file-validate-type",
      "filepond-plugin-image-exif-orientation",
      "filepond-plugin-image-crop",
      "filepond-plugin-image-edit",
      "filepond-plugin-image-preview",
      "filepond-plugin-image-resize",
      "filepond-plugin-image-transform",
      "imask",
      "nprogress",
      "notyf",
      "mapbox-gl",
      "photoswipe/lightbox",
      "photoswipe",
      "plyr",
      "v-calendar",
      "vee-validate",
      "vue",
      "vue-scrollto",
      "vue3-apexcharts",
      "vue-tippy",
      "vue-i18n",
      "vue-router",
      "unplugin-vue-router/runtime",
      "simplebar",
      "simple-datatables",
      "tiny-slider/src/tiny-slider",
      "vue-accessible-color-picker",
      "zod",
      "@stefanprobst/remark-shiki",
      "rehype-external-links",
      "rehype-raw",
      "rehype-sanitize",
      "rehype-stringify",
      "rehype-slug",
      "rehype-autolink-headings",
      "remark-gfm",
      "remark-parse",
      "remark-rehype",
      "shiki-es",
      "unified",
      "workbox-window",
      "textarea-markdown-editor/dist/esm/bootstrap"
    ],
    disabled: false
  },
  resolve: {
    alias: [
      {
        find: "/@src/",
        replacement: `/src/`
      }
    ]
  },
  build: {
    minify: "terser",
    assetsInlineLimit: 4096 * 2,
    commonjsOptions: { include: [] }
  },
  plugins: [
    Vue({
      include: [/\.vue$/]
    }),
    VueI18nPlugin({
      include: resolve(dirname(fileURLToPath(__vite_injected_original_import_meta_url)), "./src/locales/**"),
      fullInstall: false,
      compositionOnly: true
    }),
    VueRouter({
      routesFolder: "src/pages",
      dataFetching: true
    }),
    AutoImport({
      dts: true,
      imports: ["vue", "@vueuse/core", VueRouterAutoImports]
    }),
    VitePluginVueroDoc({
      pathPrefix: "documentation",
      wrapperComponent: "DocumentationItem",
      shiki: {
        theme: {
          light: "min-light",
          dark: "github-dark"
        }
      },
      sourceMeta: {
        enabled: true,
        editProtocol: "vscode://vscode-remote/wsl+Ubuntu"
      }
    }),
    Components({
      dirs: ["documentation", "src/components", "src/layouts"],
      extensions: ["vue", "md"],
      dts: true,
      include: [/\.vue$/, /\.vue\?vue/, /\.md$/]
    }),
    PurgeIcons(),
    VitePluginFonts({
      google: {
        families: [
          {
            name: "Fira Code",
            styles: "wght@400;600"
          },
          {
            name: "Montserrat",
            styles: "wght@500;600;700;800;900"
          },
          {
            name: "Roboto",
            styles: "wght@300;400;500;600;700"
          }
        ]
      }
    }),
    !process.env.GTM_ID ? void 0 : VitePluginRadar({
      gtm: {
        id: process.env.GTM_ID
      }
    }),
    VitePWA({
      base: "/",
      includeAssets: ["favicon.svg", "favicon.ico", "robots.txt", "apple-touch-icon.png"],
      manifest: {
        name: "Vuero - A complete Vue 3 design system",
        short_name: "Vuero",
        start_url: "/?utm_source=pwa",
        display: "standalone",
        theme_color: "#ffffff",
        background_color: "#ffffff",
        icons: [
          {
            src: "pwa-192x192.png",
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: "pwa-512x512.png",
            sizes: "512x512",
            type: "image/png"
          },
          {
            src: "pwa-512x512.png",
            sizes: "512x512",
            type: "image/png",
            purpose: "any maskable"
          }
        ]
      }
    }),
    purgecss({
      output: false,
      content: [`./src/**/*.vue`],
      variables: false,
      safelist: {
        standard: [
          /(autv|lnil|lnir|fas?)/,
          /-(leave|enter|appear)(|-(to|from|active))$/,
          /^(?!(|.*?:)cursor-move).+-move$/,
          /^router-link(|-exact)-active$/,
          /data-v-.*/
        ]
      },
      defaultExtractor(content) {
        const contentWithoutStyleBlocks = content.replace(/<style[^]+?<\/style>/gi, "");
        return contentWithoutStyleBlocks.match(/[A-Za-z0-9-_/:]*[A-Za-z0-9-_/]+/g) || [];
      }
    }),
    !MINIFY_IMAGES ? void 0 : ImageMin({
      gifsicle: {
        optimizationLevel: 7,
        interlaced: false
      },
      optipng: {
        optimizationLevel: 7
      },
      mozjpeg: {
        quality: 60
      },
      pngquant: {
        quality: [0.8, 0.9],
        speed: 4
      },
      svgo: {
        plugins: [
          {
            name: "removeViewBox",
            active: false
          },
          {
            name: "removeEmptyAttrs",
            active: false
          }
        ]
      }
    })
  ]
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiLCAidml0ZS1wbHVnaW4tdnVlcm8tZG9jL2luZGV4LnRzIiwgInZpdGUtcGx1Z2luLXZ1ZXJvLWRvYy9tYXJrZG93bi50cyIsICJ2aXRlLXBsdWdpbi12dWVyby1kb2MvdHJhbnNmb3JtLnRzIl0sCiAgInNvdXJjZXNDb250ZW50IjogWyJjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZGlybmFtZSA9IFwiRDpcXFxcV2Vic3Rvcm1Qcm9qZWN0c1xcXFx0ZW1wbGF0ZVZ1ZXJvXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ZpbGVuYW1lID0gXCJEOlxcXFxXZWJzdG9ybVByb2plY3RzXFxcXHRlbXBsYXRlVnVlcm9cXFxcdml0ZS5jb25maWcudHNcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfaW1wb3J0X21ldGFfdXJsID0gXCJmaWxlOi8vL0Q6L1dlYnN0b3JtUHJvamVjdHMvdGVtcGxhdGVWdWVyby92aXRlLmNvbmZpZy50c1wiO2ltcG9ydCB7IHJlc29sdmUsIGRpcm5hbWUgfSBmcm9tICdub2RlOnBhdGgnXG5pbXBvcnQgeyBmaWxlVVJMVG9QYXRoIH0gZnJvbSAnbm9kZTp1cmwnXG5pbXBvcnQgeyBkZWZpbmVDb25maWcgfSBmcm9tICd2aXRlJ1xuaW1wb3J0IFZ1ZSBmcm9tICdAdml0ZWpzL3BsdWdpbi12dWUnXG5pbXBvcnQgVnVlUm91dGVyIGZyb20gJ3VucGx1Z2luLXZ1ZS1yb3V0ZXIvdml0ZSdcbmltcG9ydCB7IFZ1ZVJvdXRlckF1dG9JbXBvcnRzIH0gZnJvbSAndW5wbHVnaW4tdnVlLXJvdXRlcidcbmltcG9ydCBDb21wb25lbnRzIGZyb20gJ3VucGx1Z2luLXZ1ZS1jb21wb25lbnRzL3ZpdGUnXG5pbXBvcnQgQXV0b0ltcG9ydCBmcm9tICd1bnBsdWdpbi1hdXRvLWltcG9ydC92aXRlJ1xuaW1wb3J0IHsgVml0ZVBsdWdpbkZvbnRzIH0gZnJvbSAndml0ZS1wbHVnaW4tZm9udHMnXG5pbXBvcnQgeyBWaXRlUGx1Z2luUmFkYXIgfSBmcm9tICd2aXRlLXBsdWdpbi1yYWRhcidcbmltcG9ydCBQdXJnZUljb25zIGZyb20gJ3ZpdGUtcGx1Z2luLXB1cmdlLWljb25zJ1xuaW1wb3J0IEltYWdlTWluIGZyb20gJ3ZpdGUtcGx1Z2luLWltYWdlbWluJ1xuaW1wb3J0IFZ1ZUkxOG5QbHVnaW4gZnJvbSAnQGludGxpZnkvdW5wbHVnaW4tdnVlLWkxOG4vdml0ZSdcbmltcG9ydCB7IFZpdGVQV0EgfSBmcm9tICd2aXRlLXBsdWdpbi1wd2EnXG5pbXBvcnQgcHVyZ2Vjc3MgZnJvbSAncm9sbHVwLXBsdWdpbi1wdXJnZWNzcydcblxuLy8gbG9jYWwgdml0ZSBwbHVnaW5cbmltcG9ydCB7IFZpdGVQbHVnaW5WdWVyb0RvYyB9IGZyb20gJy4vdml0ZS1wbHVnaW4tdnVlcm8tZG9jJ1xuXG4vLyBvcHRpb25zIHZpYSBlbnYgdmFyaWFibGVzXG5jb25zdCBNSU5JRllfSU1BR0VTID0gcHJvY2Vzcy5lbnYuTUlOSUZZID8gcHJvY2Vzcy5lbnYuTUlOSUZZID09PSAndHJ1ZScgOiBmYWxzZVxuXG4vKipcbiAqIFRoaXMgaXMgdGhlIG1haW4gY29uZmlndXJhdGlvbiBmaWxlIGZvciB2aXRlanNcbiAqXG4gKiBAc2VlIGh0dHBzOi8vdml0ZWpzLmRldi9jb25maWdcbiAqL1xuZXhwb3J0IGRlZmF1bHQgZGVmaW5lQ29uZmlnKHtcbiAgLy8gUHJvamVjdCByb290IGRpcmVjdG9yeSAod2hlcmUgaW5kZXguaHRtbCBpcyBsb2NhdGVkKS5cbiAgcm9vdDogcHJvY2Vzcy5jd2QoKSxcbiAgLy8gQmFzZSBwdWJsaWMgcGF0aCB3aGVuIHNlcnZlZCBpbiBkZXZlbG9wbWVudCBvciBwcm9kdWN0aW9uLlxuICAvLyBZb3UgYWxzbyBuZWVkIHRvIGFkZCB0aGlzIGJhc2UgbGlrZSBgaGlzdG9yeTogY3JlYXRlV2ViSGlzdG9yeSgnbXktc3ViZGlyZWN0b3J5JylgXG4gIC8vIGluIC4vc3JjL3JvdXRlci50c1xuICAvLyBiYXNlOiAnL215LXN1YmRpcmVjdG9yeS8nLFxuICBiYXNlOiAnLycsXG4gIC8vIERpcmVjdG9yeSB0byBzZXJ2ZSBhcyBwbGFpbiBzdGF0aWMgYXNzZXRzLlxuICBwdWJsaWNEaXI6ICdwdWJsaWMnLFxuICAvLyBBZGp1c3QgY29uc29sZSBvdXRwdXQgdmVyYm9zaXR5LlxuICBsb2dMZXZlbDogJ2luZm8nLFxuICAvLyBkZXZlbG9wbWVudCBzZXJ2ZXIgY29uZmlndXJhdGlvblxuICBzZXJ2ZXI6IHtcbiAgICAvLyBWaXRlIDQgZGVmYXVsdHMgdG8gNTE3MywgYnV0IHlvdSBjYW4gb3ZlcnJpZGUgaXQgd2l0aCB0aGUgcG9ydCBvcHRpb24uXG4gICAgcG9ydDogMzAwMCxcbiAgfSxcbiAgLyoqXG4gICAqIEJ5IGRlZmF1bHQsIFZpdGUgd2lsbCBjcmF3bCB5b3VyIGluZGV4Lmh0bWwgdG8gZGV0ZWN0IGRlcGVuZGVuY2llcyB0aGF0XG4gICAqIG5lZWQgdG8gYmUgcHJlLWJ1bmRsZWQuIElmIGJ1aWxkLnJvbGx1cE9wdGlvbnMuaW5wdXQgaXMgc3BlY2lmaWVkLFxuICAgKiBWaXRlIHdpbGwgY3Jhd2wgdGhvc2UgZW50cnkgcG9pbnRzIGluc3RlYWQuXG4gICAqXG4gICAqIEBzZWUgaHR0cHM6Ly92aXRlanMuZGV2L2NvbmZpZy8jb3B0aW1pemVkZXBzLWVudHJpZXNcbiAgICovXG4gIG9wdGltaXplRGVwczoge1xuICAgIGluY2x1ZGU6IFtcbiAgICAgICdAY2tlZGl0b3IvY2tlZGl0b3I1LXZ1ZScsXG4gICAgICAnQGNrZWRpdG9yL2NrZWRpdG9yNS1idWlsZC1jbGFzc2ljJyxcbiAgICAgICdAaWNvbmlmeS9pY29uaWZ5JyxcbiAgICAgICdAbWFwYm94L21hcGJveC1nbC1nZW9jb2Rlci9kaXN0L21hcGJveC1nbC1nZW9jb2Rlci5taW4uanMnLFxuICAgICAgJ0B2ZWUtdmFsaWRhdGUvem9kJyxcbiAgICAgICdAdnVldXNlL2NvcmUnLFxuICAgICAgJ0B2dWV1c2UvaGVhZCcsXG4gICAgICAnQHZ1ZWZvcm0vbXVsdGlzZWxlY3QnLFxuICAgICAgJ0B2dWVmb3JtL3NsaWRlcicsXG4gICAgICAnYXhpb3MnLFxuICAgICAgJ2JpbGxib2FyZC5qcycsXG4gICAgICAnZGF5anMnLFxuICAgICAgJ2Ryb3B6b25lJyxcbiAgICAgICdkcmFndWxhJyxcbiAgICAgICdkZWZ1JyxcbiAgICAgICdmaWxlcG9uZCcsXG4gICAgICAnZmlsZXBvbmQtcGx1Z2luLWZpbGUtdmFsaWRhdGUtc2l6ZScsXG4gICAgICAnZmlsZXBvbmQtcGx1Z2luLWZpbGUtdmFsaWRhdGUtdHlwZScsXG4gICAgICAnZmlsZXBvbmQtcGx1Z2luLWltYWdlLWV4aWYtb3JpZW50YXRpb24nLFxuICAgICAgJ2ZpbGVwb25kLXBsdWdpbi1pbWFnZS1jcm9wJyxcbiAgICAgICdmaWxlcG9uZC1wbHVnaW4taW1hZ2UtZWRpdCcsXG4gICAgICAnZmlsZXBvbmQtcGx1Z2luLWltYWdlLXByZXZpZXcnLFxuICAgICAgJ2ZpbGVwb25kLXBsdWdpbi1pbWFnZS1yZXNpemUnLFxuICAgICAgJ2ZpbGVwb25kLXBsdWdpbi1pbWFnZS10cmFuc2Zvcm0nLFxuICAgICAgJ2ltYXNrJyxcbiAgICAgICducHJvZ3Jlc3MnLFxuICAgICAgJ25vdHlmJyxcbiAgICAgICdtYXBib3gtZ2wnLFxuICAgICAgJ3Bob3Rvc3dpcGUvbGlnaHRib3gnLFxuICAgICAgJ3Bob3Rvc3dpcGUnLFxuICAgICAgJ3BseXInLFxuICAgICAgJ3YtY2FsZW5kYXInLFxuICAgICAgJ3ZlZS12YWxpZGF0ZScsXG4gICAgICAndnVlJyxcbiAgICAgICd2dWUtc2Nyb2xsdG8nLFxuICAgICAgJ3Z1ZTMtYXBleGNoYXJ0cycsXG4gICAgICAndnVlLXRpcHB5JyxcbiAgICAgICd2dWUtaTE4bicsXG4gICAgICAndnVlLXJvdXRlcicsXG4gICAgICAndW5wbHVnaW4tdnVlLXJvdXRlci9ydW50aW1lJyxcbiAgICAgICdzaW1wbGViYXInLFxuICAgICAgJ3NpbXBsZS1kYXRhdGFibGVzJyxcbiAgICAgICd0aW55LXNsaWRlci9zcmMvdGlueS1zbGlkZXInLFxuICAgICAgJ3Z1ZS1hY2Nlc3NpYmxlLWNvbG9yLXBpY2tlcicsXG4gICAgICAnem9kJyxcbiAgICAgICdAc3RlZmFucHJvYnN0L3JlbWFyay1zaGlraScsXG4gICAgICAncmVoeXBlLWV4dGVybmFsLWxpbmtzJyxcbiAgICAgICdyZWh5cGUtcmF3JyxcbiAgICAgICdyZWh5cGUtc2FuaXRpemUnLFxuICAgICAgJ3JlaHlwZS1zdHJpbmdpZnknLFxuICAgICAgJ3JlaHlwZS1zbHVnJyxcbiAgICAgICdyZWh5cGUtYXV0b2xpbmstaGVhZGluZ3MnLFxuICAgICAgJ3JlbWFyay1nZm0nLFxuICAgICAgJ3JlbWFyay1wYXJzZScsXG4gICAgICAncmVtYXJrLXJlaHlwZScsXG4gICAgICAnc2hpa2ktZXMnLFxuICAgICAgJ3VuaWZpZWQnLFxuICAgICAgJ3dvcmtib3gtd2luZG93JyxcbiAgICAgICd0ZXh0YXJlYS1tYXJrZG93bi1lZGl0b3IvZGlzdC9lc20vYm9vdHN0cmFwJyxcbiAgICBdLFxuICAgIGRpc2FibGVkOiBmYWxzZSxcbiAgfSxcbiAgLy8gV2lsbCBiZSBwYXNzZWQgdG8gQHJvbGx1cC9wbHVnaW4tYWxpYXMgYXMgaXRzIGVudHJpZXMgb3B0aW9uLlxuICByZXNvbHZlOiB7XG4gICAgYWxpYXM6IFtcbiAgICAgIHtcbiAgICAgICAgZmluZDogJy9Ac3JjLycsXG4gICAgICAgIHJlcGxhY2VtZW50OiBgL3NyYy9gLFxuICAgICAgfSxcbiAgICBdLFxuICB9LFxuICBidWlsZDoge1xuICAgIG1pbmlmeTogJ3RlcnNlcicsXG4gICAgLy8gRG8gbm90IHdhcm4gYWJvdXQgbGFyZ2UgY2h1bmtzXG4gICAgLy8gY2h1bmtTaXplV2FybmluZ0xpbWl0OiBJbmZpbml0eSxcbiAgICAvLyBEb3VibGUgdGhlIGRlZmF1bHQgc2l6ZSB0aHJlc2hvbGQgZm9yIGlubGluZWQgYXNzZXRzXG4gICAgLy8gaHR0cHM6Ly92aXRlanMuZGV2L2NvbmZpZy9idWlsZC1vcHRpb25zLmh0bWwjYnVpbGQtYXNzZXRzaW5saW5lbGltaXRcbiAgICBhc3NldHNJbmxpbmVMaW1pdDogNDA5NiAqIDIsXG4gICAgY29tbW9uanNPcHRpb25zOiB7IGluY2x1ZGU6IFtdIH0sXG4gIH0sXG4gIHBsdWdpbnM6IFtcbiAgICAvKipcbiAgICAgKiBwbHVnaW4tdnVlIHBsdWdpbiBpbmplY3QgdnVlIGxpYnJhcnkgYW5kIGFsbG93IHNmYyBmaWxlcyB0byB3b3JrICgqLnZ1ZSlcbiAgICAgKlxuICAgICAqIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL3ZpdGVqcy92aXRlL3RyZWUvbWFpbi9wYWNrYWdlcy9wbHVnaW4tdnVlXG4gICAgICovXG4gICAgVnVlKHtcbiAgICAgIGluY2x1ZGU6IFsvXFwudnVlJC9dLFxuICAgIH0pLFxuXG4gICAgLyoqXG4gICAgICogdml0ZS1wbHVnaW4tdnVlLWkxOG4gcGx1Z2luIGRvZXMgaTE4biByZXNvdXJjZXMgcHJlLWNvbXBpbGF0aW9uIC8gb3B0aW1pemF0aW9uc1xuICAgICAqXG4gICAgICogQHNlZSBodHRwczovL2dpdGh1Yi5jb20vaW50bGlmeS9idW5kbGUtdG9vbHMvdHJlZS9tYWluL3BhY2thZ2VzL3ZpdGUtcGx1Z2luLXZ1ZS1pMThuXG4gICAgICovXG4gICAgVnVlSTE4blBsdWdpbih7XG4gICAgICBpbmNsdWRlOiByZXNvbHZlKGRpcm5hbWUoZmlsZVVSTFRvUGF0aChpbXBvcnQubWV0YS51cmwpKSwgJy4vc3JjL2xvY2FsZXMvKionKSxcbiAgICAgIGZ1bGxJbnN0YWxsOiBmYWxzZSxcbiAgICAgIGNvbXBvc2l0aW9uT25seTogdHJ1ZSxcbiAgICB9KSxcblxuICAgIC8qKlxuICAgICAqIHVucGx1Z2luLXZ1ZS1yb3V0ZXIgcGx1Z2luIGdlbmVyYXRlIHJvdXRlcyBiYXNlZCBvbiBmaWxlIHN5c3RlbVxuICAgICAqIGFsbG93IHRvIHVzZSB0eXBlZCByb3V0ZXMgYW5kIHVzYWdlIG9mIGRlZmluZUxvYWRlclxuICAgICAqXG4gICAgICogQHNlZSBodHRwczovL2dpdGh1Yi5jb20vcG9zdmEvdW5wbHVnaW4tdnVlLXJvdXRlclxuICAgICAqIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL3Z1ZWpzL3JmY3MvYmxvYi9hZDY5ZGEyYWVlOTI0MmVmODhmMDM2NzEzZGI2OGYzZWYyNzRiYjFiL2FjdGl2ZS1yZmNzLzAwMDAtcm91dGVyLXVzZS1sb2FkZXIubWRcbiAgICAgKi9cbiAgICBWdWVSb3V0ZXIoe1xuICAgICAgcm91dGVzRm9sZGVyOiAnc3JjL3BhZ2VzJyxcblxuICAgICAgLyoqXG4gICAgICAgKiBEYXRhIEZldGNoaW5nIGlzIGFuIGV4cGVyaW1lbnRhbCBmZWF0dXJlIGZyb20gdnVlICYgdnVlLXJvdXRlclxuICAgICAgICpcbiAgICAgICAqIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL3Z1ZWpzL3JmY3MvZGlzY3Vzc2lvbnMvNDYwXG4gICAgICAgKiBAc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9wb3N2YS91bnBsdWdpbi12dWUtcm91dGVyL3RyZWUvbWFpbi9zcmMvZGF0YS1mZXRjaGluZ1xuICAgICAgICovXG4gICAgICBkYXRhRmV0Y2hpbmc6IHRydWUsXG4gICAgfSksXG5cbiAgICAvKipcbiAgICAgKiB1bnBsdWdpbi1hdXRvLWltcG9ydCBhbGxvdyB0byBhdXRvbWF0aWNhbHkgaW1wb3J0IG1vZHVsZXMvY29tcG9uZW50c1xuICAgICAqXG4gICAgICogQHNlZSBodHRwczovL2dpdGh1Yi5jb20vYW50ZnUvdW5wbHVnaW4tYXV0by1pbXBvcnRcbiAgICAgKi9cbiAgICBBdXRvSW1wb3J0KHtcbiAgICAgIGR0czogdHJ1ZSxcbiAgICAgIGltcG9ydHM6IFsndnVlJywgJ0B2dWV1c2UvY29yZScsIFZ1ZVJvdXRlckF1dG9JbXBvcnRzXSxcbiAgICB9KSxcblxuICAgIC8qKlxuICAgICAqIFRoaXMgaXMgYW4gaW50ZXJuYWwgdml0ZSBwbHVnaW4gdGhhdCBsb2FkIG1hcmtkb3duIGZpbGVzIGFzIHZ1ZSBjb21wb25lbnRzLlxuICAgICAqXG4gICAgICogQHNlZSAvZG9jdW1lbnRhdGlvblxuICAgICAqIEBzZWUgL3ZpdGUtcGx1Z2luLXZ1ZXJvLWRvY1xuICAgICAqIEBzZWUgL3NyYy9jb21wb25lbnRzL3BhcnRpYWxzL2RvY3VtZW50YXRpb24vRG9jdW1lbnRhdGlvbkl0ZW0udnVlXG4gICAgICogQHNlZSAvc3JjL2NvbXBvc2FibGUvdXNlTWFya2Rvd25Ub2MudHNcbiAgICAgKi9cbiAgICBWaXRlUGx1Z2luVnVlcm9Eb2Moe1xuICAgICAgcGF0aFByZWZpeDogJ2RvY3VtZW50YXRpb24nLFxuICAgICAgd3JhcHBlckNvbXBvbmVudDogJ0RvY3VtZW50YXRpb25JdGVtJyxcbiAgICAgIHNoaWtpOiB7XG4gICAgICAgIHRoZW1lOiB7XG4gICAgICAgICAgbGlnaHQ6ICdtaW4tbGlnaHQnLFxuICAgICAgICAgIGRhcms6ICdnaXRodWItZGFyaycsXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgc291cmNlTWV0YToge1xuICAgICAgICBlbmFibGVkOiB0cnVlLFxuICAgICAgICBlZGl0UHJvdG9jb2w6ICd2c2NvZGU6Ly92c2NvZGUtcmVtb3RlL3dzbCtVYnVudHUnLCAvLyBvciAndnNjb2RlOi8vZmlsZSdcbiAgICAgIH0sXG4gICAgfSksXG5cbiAgICAvKipcbiAgICAgKiB1bnBsdWdpbi12dWUtY29tcG9uZW50cyBwbHVnaW4gaXMgcmVzcG9uc2libGUgb2YgYXV0b2xvYWRpbmcgY29tcG9uZW50c1xuICAgICAqIGRvY3VtZW50YXRpb24gYW5kIG1kIGZpbGUgYXJlIGxvYWRlZCBmb3IgZWxlbWVudHMgYW5kIGNvbXBvbmVudHMgc2VjdGlvbnNcbiAgICAgKlxuICAgICAqIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL2FudGZ1L3VucGx1Z2luLXZ1ZS1jb21wb25lbnRzXG4gICAgICovXG4gICAgQ29tcG9uZW50cyh7XG4gICAgICBkaXJzOiBbJ2RvY3VtZW50YXRpb24nLCAnc3JjL2NvbXBvbmVudHMnLCAnc3JjL2xheW91dHMnXSxcbiAgICAgIGV4dGVuc2lvbnM6IFsndnVlJywgJ21kJ10sXG4gICAgICBkdHM6IHRydWUsXG4gICAgICBpbmNsdWRlOiBbL1xcLnZ1ZSQvLCAvXFwudnVlXFw/dnVlLywgL1xcLm1kJC9dLFxuICAgIH0pLFxuXG4gICAgLyoqXG4gICAgICogdml0ZS1wbHVnaW4tcHVyZ2UtaWNvbnMgcGx1Z2luIGlzIHJlc3BvbnNpYmxlIG9mIGF1dG9sb2FkaW5nIGljb25lcyBmcm9tIG11bHRpcGxlcyBwcm92aWRlcnNcbiAgICAgKlxuICAgICAqIEBzZWUgaHR0cHM6Ly9pY29uZXMubmV0bGlmeS5hcHAvXG4gICAgICogQHNlZSBodHRwczovL2dpdGh1Yi5jb20vYW50ZnUvcHVyZ2UtaWNvbnMvdHJlZS9tYWluL3BhY2thZ2VzL3ZpdGUtcGx1Z2luLXB1cmdlLWljb25zXG4gICAgICovXG4gICAgUHVyZ2VJY29ucygpLFxuXG4gICAgLyoqXG4gICAgICogdml0ZS1wbHVnaW4tZm9udHMgcGx1Z2luIGluamVjdCB3ZWJmb250cyBmcm9tIGRpZmZlcmVudHMgcHJvdmlkZXJzXG4gICAgICpcbiAgICAgKiBAc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9zdGFmeW5pYWtzYWNoYS92aXRlLXBsdWdpbi1mb250c1xuICAgICAqL1xuICAgIFZpdGVQbHVnaW5Gb250cyh7XG4gICAgICBnb29nbGU6IHtcbiAgICAgICAgZmFtaWxpZXM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnRmlyYSBDb2RlJyxcbiAgICAgICAgICAgIHN0eWxlczogJ3dnaHRANDAwOzYwMCcsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnTW9udHNlcnJhdCcsXG4gICAgICAgICAgICBzdHlsZXM6ICd3Z2h0QDUwMDs2MDA7NzAwOzgwMDs5MDAnLFxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ1JvYm90bycsXG4gICAgICAgICAgICBzdHlsZXM6ICd3Z2h0QDMwMDs0MDA7NTAwOzYwMDs3MDAnLFxuICAgICAgICAgIH0sXG4gICAgICAgIF0sXG4gICAgICB9LFxuICAgIH0pLFxuXG4gICAgLyoqXG4gICAgICogdml0ZS1wbHVnaW4tcmFkYXIgcGx1Z2luIGluamVjdCBzbmlwcGV0cyBmcm9tIGFuYWx5dGljcyBwcm92aWRlcnNcbiAgICAgKlxuICAgICAqIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL3N0YWZ5bmlha3NhY2hhL3ZpdGUtcGx1Z2luLXJhZGFyXG4gICAgICovXG4gICAgIXByb2Nlc3MuZW52LkdUTV9JRFxuICAgICAgPyB1bmRlZmluZWRcbiAgICAgIDogVml0ZVBsdWdpblJhZGFyKHtcbiAgICAgICAgICBndG06IHtcbiAgICAgICAgICAgIGlkOiBwcm9jZXNzLmVudi5HVE1fSUQsXG4gICAgICAgICAgfSxcbiAgICAgICAgfSksXG5cbiAgICAvKipcbiAgICAgKiB2aXRlLXBsdWdpbi1wd2EgZ2VuZXJhdGUgbWFuaWZlc3QuanNvbiBhbmQgcmVnaXN0ZXIgc2VydmljZXMgd29ya2VyIHRvIGVuYWJsZSBQV0FcbiAgICAgKlxuICAgICAqIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL2FudGZ1L3ZpdGUtcGx1Z2luLXB3YVxuICAgICAqL1xuICAgIFZpdGVQV0Eoe1xuICAgICAgYmFzZTogJy8nLFxuICAgICAgaW5jbHVkZUFzc2V0czogWydmYXZpY29uLnN2ZycsICdmYXZpY29uLmljbycsICdyb2JvdHMudHh0JywgJ2FwcGxlLXRvdWNoLWljb24ucG5nJ10sXG4gICAgICBtYW5pZmVzdDoge1xuICAgICAgICBuYW1lOiAnVnVlcm8gLSBBIGNvbXBsZXRlIFZ1ZSAzIGRlc2lnbiBzeXN0ZW0nLFxuICAgICAgICBzaG9ydF9uYW1lOiAnVnVlcm8nLFxuICAgICAgICBzdGFydF91cmw6ICcvP3V0bV9zb3VyY2U9cHdhJyxcbiAgICAgICAgZGlzcGxheTogJ3N0YW5kYWxvbmUnLFxuICAgICAgICB0aGVtZV9jb2xvcjogJyNmZmZmZmYnLFxuICAgICAgICBiYWNrZ3JvdW5kX2NvbG9yOiAnI2ZmZmZmZicsXG4gICAgICAgIGljb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgc3JjOiAncHdhLTE5MngxOTIucG5nJyxcbiAgICAgICAgICAgIHNpemVzOiAnMTkyeDE5MicsXG4gICAgICAgICAgICB0eXBlOiAnaW1hZ2UvcG5nJyxcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHNyYzogJ3B3YS01MTJ4NTEyLnBuZycsXG4gICAgICAgICAgICBzaXplczogJzUxMng1MTInLFxuICAgICAgICAgICAgdHlwZTogJ2ltYWdlL3BuZycsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBzcmM6ICdwd2EtNTEyeDUxMi5wbmcnLFxuICAgICAgICAgICAgc2l6ZXM6ICc1MTJ4NTEyJyxcbiAgICAgICAgICAgIHR5cGU6ICdpbWFnZS9wbmcnLFxuICAgICAgICAgICAgcHVycG9zZTogJ2FueSBtYXNrYWJsZScsXG4gICAgICAgICAgfSxcbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgfSksXG5cbiAgICAvKipcbiAgICAgKiByb2xsdXAtcGx1Z2luLXB1cmdlY3NzIHBsdWdpbiBpcyByZXNwb25zaWJsZSBvZiBwdXJnaW5nIGNzcyBydWxlc1xuICAgICAqIHRoYXQgYXJlIG5vdCB1c2VkIGluIHRoZSBidW5kbGVcbiAgICAgKlxuICAgICAqIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL0Z1bGxIdW1hbi9wdXJnZWNzcy90cmVlL21haW4vcGFja2FnZXMvcm9sbHVwLXBsdWdpbi1wdXJnZWNzc1xuICAgICAqL1xuICAgIHB1cmdlY3NzKHtcbiAgICAgIG91dHB1dDogZmFsc2UsXG4gICAgICBjb250ZW50OiBbYC4vc3JjLyoqLyoudnVlYF0sXG4gICAgICB2YXJpYWJsZXM6IGZhbHNlLFxuICAgICAgc2FmZWxpc3Q6IHtcbiAgICAgICAgc3RhbmRhcmQ6IFtcbiAgICAgICAgICAvKGF1dHZ8bG5pbHxsbmlyfGZhcz8pLyxcbiAgICAgICAgICAvLShsZWF2ZXxlbnRlcnxhcHBlYXIpKHwtKHRvfGZyb218YWN0aXZlKSkkLyxcbiAgICAgICAgICAvXig/ISh8Lio/OiljdXJzb3ItbW92ZSkuKy1tb3ZlJC8sXG4gICAgICAgICAgL15yb3V0ZXItbGluayh8LWV4YWN0KS1hY3RpdmUkLyxcbiAgICAgICAgICAvZGF0YS12LS4qLyxcbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgICBkZWZhdWx0RXh0cmFjdG9yKGNvbnRlbnQpIHtcbiAgICAgICAgY29uc3QgY29udGVudFdpdGhvdXRTdHlsZUJsb2NrcyA9IGNvbnRlbnQucmVwbGFjZSgvPHN0eWxlW15dKz88XFwvc3R5bGU+L2dpLCAnJylcbiAgICAgICAgcmV0dXJuIGNvbnRlbnRXaXRob3V0U3R5bGVCbG9ja3MubWF0Y2goL1tBLVphLXowLTktXy86XSpbQS1aYS16MC05LV8vXSsvZykgfHwgW11cbiAgICAgIH0sXG4gICAgfSksXG5cbiAgICAvKipcbiAgICAgKiB2aXRlLXBsdWdpbi1pbWFnZW1pbiBvcHRpbWl6ZSBhbGwgaW1hZ2VzIHNpemVzIGZyb20gcHVibGljIG9yIGFzc2V0IGZvbGRlclxuICAgICAqXG4gICAgICogQHNlZSBodHRwczovL2dpdGh1Yi5jb20vYW5uY3diL3ZpdGUtcGx1Z2luLWltYWdlbWluXG4gICAgICovXG4gICAgIU1JTklGWV9JTUFHRVNcbiAgICAgID8gdW5kZWZpbmVkXG4gICAgICA6IEltYWdlTWluKHtcbiAgICAgICAgICBnaWZzaWNsZToge1xuICAgICAgICAgICAgb3B0aW1pemF0aW9uTGV2ZWw6IDcsXG4gICAgICAgICAgICBpbnRlcmxhY2VkOiBmYWxzZSxcbiAgICAgICAgICB9LFxuICAgICAgICAgIG9wdGlwbmc6IHtcbiAgICAgICAgICAgIG9wdGltaXphdGlvbkxldmVsOiA3LFxuICAgICAgICAgIH0sXG4gICAgICAgICAgbW96anBlZzoge1xuICAgICAgICAgICAgcXVhbGl0eTogNjAsXG4gICAgICAgICAgfSxcbiAgICAgICAgICBwbmdxdWFudDoge1xuICAgICAgICAgICAgcXVhbGl0eTogWzAuOCwgMC45XSxcbiAgICAgICAgICAgIHNwZWVkOiA0LFxuICAgICAgICAgIH0sXG4gICAgICAgICAgc3Znbzoge1xuICAgICAgICAgICAgcGx1Z2luczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbmFtZTogJ3JlbW92ZVZpZXdCb3gnLFxuICAgICAgICAgICAgICAgIGFjdGl2ZTogZmFsc2UsXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBuYW1lOiAncmVtb3ZlRW1wdHlBdHRycycsXG4gICAgICAgICAgICAgICAgYWN0aXZlOiBmYWxzZSxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfSksXG4gIF0sXG59KVxuIiwgImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJEOlxcXFxXZWJzdG9ybVByb2plY3RzXFxcXHRlbXBsYXRlVnVlcm9cXFxcdml0ZS1wbHVnaW4tdnVlcm8tZG9jXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ZpbGVuYW1lID0gXCJEOlxcXFxXZWJzdG9ybVByb2plY3RzXFxcXHRlbXBsYXRlVnVlcm9cXFxcdml0ZS1wbHVnaW4tdnVlcm8tZG9jXFxcXGluZGV4LnRzXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ltcG9ydF9tZXRhX3VybCA9IFwiZmlsZTovLy9EOi9XZWJzdG9ybVByb2plY3RzL3RlbXBsYXRlVnVlcm8vdml0ZS1wbHVnaW4tdnVlcm8tZG9jL2luZGV4LnRzXCI7aW1wb3J0IHR5cGUgeyBQbHVnaW4sIFJlc29sdmVkQ29uZmlnIH0gZnJvbSAndml0ZSdcbmltcG9ydCB0eXBlIHsgUHJvY2Vzc29yIH0gZnJvbSAndW5pZmllZCdcbmltcG9ydCB0eXBlIHsgVGhlbWUgfSBmcm9tICdzaGlraS1lcydcblxuaW1wb3J0IHsgam9pbiwgYmFzZW5hbWUgfSBmcm9tICdwYXRoZSdcbmltcG9ydCB7IGNvbXBpbGVUZW1wbGF0ZSwgcGFyc2UgfSBmcm9tICdAdnVlL2NvbXBpbGVyLXNmYydcblxuaW1wb3J0IHsgY3JlYXRlUHJvY2Vzc29ycyB9IGZyb20gJy4vbWFya2Rvd24nXG5pbXBvcnQgeyB0cmFuc2Zvcm1FeGFtcGxlTWFya3VwLCB0cmFuc2Zvcm1TbG90cyB9IGZyb20gJy4vdHJhbnNmb3JtJ1xuXG5mdW5jdGlvbiBwYXJzZUlkKGlkOiBzdHJpbmcpIHtcbiAgY29uc3QgaW5kZXggPSBpZC5pbmRleE9mKCc/JylcbiAgaWYgKGluZGV4IDwgMCkgcmV0dXJuIGlkXG4gIGVsc2UgcmV0dXJuIGlkLnNsaWNlKDAsIGluZGV4KVxufVxuXG5leHBvcnQgaW50ZXJmYWNlIFBsdWdpbk9wdGlvbnMge1xuICBwYXRoUHJlZml4Pzogc3RyaW5nXG4gIHdyYXBwZXJDb21wb25lbnQ6IHN0cmluZ1xuICBzaGlraToge1xuICAgIHRoZW1lOlxuICAgICAgfCBUaGVtZVxuICAgICAgfCB7XG4gICAgICAgICAgbGlnaHQ6IFRoZW1lXG4gICAgICAgICAgZGFyazogVGhlbWVcbiAgICAgICAgfVxuICB9XG4gIHNvdXJjZU1ldGE/OiB7XG4gICAgZW5hYmxlZD86IGJvb2xlYW5cbiAgICBlZGl0UHJvdG9jb2w/OiBzdHJpbmdcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gVml0ZVBsdWdpblZ1ZXJvRG9jKG9wdGlvbnM6IFBsdWdpbk9wdGlvbnMpIHtcbiAgbGV0IGNvbmZpZzogUmVzb2x2ZWRDb25maWcgfCB1bmRlZmluZWRcbiAgbGV0IHByb2Nlc3NvcnM6IHsgbGlnaHQ6IFByb2Nlc3NvcjsgZGFyazogUHJvY2Vzc29yIH0gfCB1bmRlZmluZWRcblxuICBjb25zdCBjd2QgPSBwcm9jZXNzLmN3ZCgpXG4gIGNvbnN0IHBhdGhQcmVmaXggPSBvcHRpb25zLnBhdGhQcmVmaXggPyBqb2luKGN3ZCwgb3B0aW9ucy5wYXRoUHJlZml4KSA6IGN3ZFxuXG4gIGFzeW5jIGZ1bmN0aW9uIG1hcmtkb3duVG9WdWUoaWQ6IHN0cmluZywgcmF3OiBzdHJpbmcpIHtcbiAgICBjb25zdCBwYXRoID0gcGFyc2VJZChpZClcblxuICAgIC8vIHRyYW5zZm9ybSBleGFtcGxlIG1hcmt1cCB0byB1c2Uga2ViYWItY2FzZSB3aXRob3V0IHNlbGYtY2xvc2luZyBlbGVtZW50cy5cbiAgICAvLyB0aGlzIGlzIG5lZWRlZCBiZWNhdXNlIHJlaHlwZS1yYXcgcmVxdWlyZXMgdmFsaWQgaHRtbCAob25seSBzb21lIHRhZ3MgYXJlIHNlbGYtY2xvc2FibGUpXG4gICAgY29uc3QgaW5wdXQgPSB0cmFuc2Zvcm1FeGFtcGxlTWFya3VwKHJhdylcblxuICAgIC8vIHByb2Nlc3MgbWFya2Rvd24gd2l0aCByZW1hcmtcbiAgICBpZiAoIXByb2Nlc3NvcnMpIHByb2Nlc3NvcnMgPSBhd2FpdCBjcmVhdGVQcm9jZXNzb3JzKG9wdGlvbnMuc2hpa2kudGhlbWUpXG5cbiAgICBjb25zdCBbdkZpbGVMaWdodCwgdkZpbGVEYXJrXSA9IGF3YWl0IFByb21pc2UuYWxsKFtcbiAgICAgIHByb2Nlc3NvcnMubGlnaHQucHJvY2VzcyhpbnB1dCksXG4gICAgICBwcm9jZXNzb3JzLmRhcmsucHJvY2VzcyhpbnB1dCksXG4gICAgXSlcblxuICAgIGNvbnN0IGNvbnRlbnRMaWdodCA9IHZGaWxlTGlnaHQudG9TdHJpbmcoKVxuICAgIGNvbnN0IGNvbnRlbnREYXJrID0gdkZpbGVEYXJrLnRvU3RyaW5nKClcbiAgICBjb25zdCBmcm9udG1hdHRlciA9IHZGaWxlTGlnaHQuZGF0YT8uZnJvbnRtYXR0ZXIgPz8ge31cblxuICAgIC8vIHJlcGxhY2UgY29kZS9leGFtcGxlIHNsb3RzIHBsYWNlaG9sZGVyc1xuICAgIGNvbnN0IHNsb3RMaWdodCA9IHRyYW5zZm9ybVNsb3RzKGNvbnRlbnRMaWdodCwgJ3YtaWY9XCIhZGFya21vZGUuaXNEYXJrXCInKVxuICAgIGNvbnN0IHNsb3REYXJrID0gdHJhbnNmb3JtU2xvdHMoY29udGVudERhcmssICd2LWlmPVwiZGFya21vZGUuaXNEYXJrXCInKVxuXG4gICAgLy8gd3JhcCBjb250ZW50IGluIHdyYXBwZXIgY29tcG9uZW50IGRlZmF1bHQgc2xvdFxuICAgIGNvbnN0IHNmYyA9IFtcbiAgICAgIGA8dGVtcGxhdGU+YCxcbiAgICAgIGAgIDwke29wdGlvbnMud3JhcHBlckNvbXBvbmVudH0gOmZyb250bWF0dGVyPVwiZnJvbnRtYXR0ZXJcIiA6c291cmNlLW1ldGE9XCJzb3VyY2VNZXRhXCI+YCxcbiAgICAgIGAgICAgJHtzbG90TGlnaHR9YCxcbiAgICAgIGAgICAgJHtzbG90RGFya31gLFxuICAgICAgYCAgPC8ke29wdGlvbnMud3JhcHBlckNvbXBvbmVudH0+YCxcbiAgICAgIGA8L3RlbXBsYXRlPmAsXG4gICAgXS5qb2luKCdcXG4nKVxuXG4gICAgLy8gcGFyc2UgdGVtcGxhdGUgd2l0aCB2dWUgc2ZjIGNvbXBpbGVyXG4gICAgY29uc3QgcmVzdWx0ID0gcGFyc2Uoc2ZjLCB7XG4gICAgICBmaWxlbmFtZTogcGF0aCxcbiAgICAgIHNvdXJjZU1hcDogdHJ1ZSxcbiAgICB9KVxuXG4gICAgaWYgKHJlc3VsdC5lcnJvcnMubGVuZ3RoIHx8IHJlc3VsdC5kZXNjcmlwdG9yLnRlbXBsYXRlID09PSBudWxsKSB7XG4gICAgICBjb25zb2xlLmVycm9yKHJlc3VsdC5lcnJvcnMpXG5cbiAgICAgIHRocm93IG5ldyBFcnJvcihgTWFya2Rvd246IHVuYWJsZSB0byBwYXJzZSB2aXJ0dWFsIGZpbGUgZ2VuZXJhdGVkIGZvciBcIiR7aWR9XCJgKVxuICAgIH1cblxuICAgIC8vIGNvbXBpbGUgdGVtcGxhdGUgd2l0aCB2dWUgc2ZjIGNvbXBpbGVyXG4gICAgY29uc3QgeyBjb2RlLCBlcnJvcnMgfSA9IGNvbXBpbGVUZW1wbGF0ZSh7XG4gICAgICBmaWxlbmFtZTogcGF0aCxcbiAgICAgIGlkOiBwYXRoLFxuICAgICAgc291cmNlOiByZXN1bHQuZGVzY3JpcHRvci50ZW1wbGF0ZS5jb250ZW50LFxuICAgICAgcHJlcHJvY2Vzc0xhbmc6IHJlc3VsdC5kZXNjcmlwdG9yLnRlbXBsYXRlLmxhbmcsXG4gICAgICB0cmFuc2Zvcm1Bc3NldFVybHM6IGZhbHNlLFxuICAgIH0pXG5cbiAgICBpZiAoZXJyb3JzLmxlbmd0aCkge1xuICAgICAgY29uc29sZS5lcnJvcihlcnJvcnMpXG5cbiAgICAgIHRocm93IG5ldyBFcnJvcihgTWFya2Rvd246IHVuYWJsZSB0byBjb21waWxlIHZpcnR1YWwgZmlsZSBcIiR7aWR9XCJgKVxuICAgIH1cblxuICAgIC8vIHNvdXJjZSBpcyB1c2VkIHRvIGRpc3BsYXkgZWRpdCBzb3VyY2UgbGluayBpbiB0aGUgZG9jc1xuICAgIGxldCBzb3VyY2VNZXRhID0gJ3VuZGVmaW5lZCdcbiAgICBpZiAob3B0aW9ucy5zb3VyY2VNZXRhPy5lbmFibGVkKSB7XG4gICAgICBzb3VyY2VNZXRhID0gSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICByZWxhdGl2ZVBhdGg6IHBhdGguc3Vic3RyaW5nKGN3ZC5sZW5ndGgpLFxuICAgICAgICBiYXNlbmFtZTogYmFzZW5hbWUocGF0aCksXG4gICAgICAgIHBhdGg6IGNvbmZpZz8uaXNQcm9kdWN0aW9uID8gJycgOiBwYXRoLFxuICAgICAgICBlZGl0UHJvdG9jb2w6IGNvbmZpZz8uaXNQcm9kdWN0aW9uID8gJycgOiBvcHRpb25zLnNvdXJjZU1ldGEuZWRpdFByb3RvY29sLFxuICAgICAgfSlcbiAgICB9XG5cbiAgICAvLyBpbmplY3QgZnJvbnRtYXR0ZXIvZGFya21vZGUgYW5kIGhtcklkIGludG8gdGhlIGNvbXBpbGVkIHJlbmRlciBmdW5jdGlvblxuICAgIGNvbnN0IG91dHB1dCA9IFtcbiAgICAgIGBpbXBvcnQgeyByZWFjdGl2ZSB9IGZyb20gJ3Z1ZSdgLFxuICAgICAgYGltcG9ydCB7IHVzZURhcmttb2RlIH0gZnJvbSAnL0BzcmMvc3RvcmVzL2Rhcmttb2RlJ2AsXG5cbiAgICAgIGNvZGUucmVwbGFjZSgnZXhwb3J0IGZ1bmN0aW9uIHJlbmRlcicsICdmdW5jdGlvbiByZW5kZXInKSxcblxuICAgICAgYGNvbnN0IF9fZnJvbnRtYXR0ZXIgPSAke0pTT04uc3RyaW5naWZ5KGZyb250bWF0dGVyKX07YCxcbiAgICAgIGBjb25zdCBzZXR1cCA9ICgpID0+ICh7YCxcbiAgICAgIGAgIGZyb250bWF0dGVyOiByZWFjdGl2ZShfX2Zyb250bWF0dGVyKSxgLFxuICAgICAgYCAgZGFya21vZGU6IHVzZURhcmttb2RlKCksYCxcbiAgICAgIGAgIHNvdXJjZU1ldGE6ICR7c291cmNlTWV0YX0sYCxcbiAgICAgIGB9KTtgLFxuICAgICAgYGNvbnN0IF9fc2NyaXB0ID0geyByZW5kZXIsIHNldHVwIH07YCxcblxuICAgICAgY29uZmlnPy5pc1Byb2R1Y3Rpb24gPyAnJyA6IGBfX3NjcmlwdC5fX2htcklkID0gJHtKU09OLnN0cmluZ2lmeShwYXRoKX07YCxcbiAgICAgIGBleHBvcnQgZGVmYXVsdCBfX3NjcmlwdDtgLFxuICAgIF0uam9pbignXFxuJylcblxuICAgIHJldHVybiBvdXRwdXRcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgbmFtZTogJ3ZpdGUtcGx1Z2luLXZ1ZXJvLWRvYycsXG4gICAgZW5mb3JjZTogJ3ByZScsXG4gICAgY29uZmlnUmVzb2x2ZWQoX2NvbmZpZykge1xuICAgICAgY29uZmlnID0gX2NvbmZpZ1xuICAgIH0sXG4gICAgYXN5bmMgdHJhbnNmb3JtKHJhdywgaWQpIHtcbiAgICAgIGlmIChpZC5lbmRzV2l0aCgnLm1kJykgJiYgaWQuc3RhcnRzV2l0aChwYXRoUHJlZml4KSkge1xuICAgICAgICByZXR1cm4gYXdhaXQgbWFya2Rvd25Ub1Z1ZShpZCwgcmF3KVxuICAgICAgfVxuICAgIH0sXG4gIH0gc2F0aXNmaWVzIFBsdWdpblxufVxuXG5leHBvcnQgZGVmYXVsdCBWaXRlUGx1Z2luVnVlcm9Eb2NcbiIsICJjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZGlybmFtZSA9IFwiRDpcXFxcV2Vic3Rvcm1Qcm9qZWN0c1xcXFx0ZW1wbGF0ZVZ1ZXJvXFxcXHZpdGUtcGx1Z2luLXZ1ZXJvLWRvY1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9maWxlbmFtZSA9IFwiRDpcXFxcV2Vic3Rvcm1Qcm9qZWN0c1xcXFx0ZW1wbGF0ZVZ1ZXJvXFxcXHZpdGUtcGx1Z2luLXZ1ZXJvLWRvY1xcXFxtYXJrZG93bi50c1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9pbXBvcnRfbWV0YV91cmwgPSBcImZpbGU6Ly8vRDovV2Vic3Rvcm1Qcm9qZWN0cy90ZW1wbGF0ZVZ1ZXJvL3ZpdGUtcGx1Z2luLXZ1ZXJvLWRvYy9tYXJrZG93bi50c1wiO2ltcG9ydCB5YW1sIGZyb20gJ2pzLXlhbWwnXG5pbXBvcnQgcmVtYXJrU2hpa2kgZnJvbSAnQHN0ZWZhbnByb2JzdC9yZW1hcmstc2hpa2knXG5pbXBvcnQgcmVoeXBlRXh0ZXJuYWxMaW5rcyBmcm9tICdyZWh5cGUtZXh0ZXJuYWwtbGlua3MnXG5pbXBvcnQgcmVoeXBlUmF3IGZyb20gJ3JlaHlwZS1yYXcnXG5pbXBvcnQgcmVoeXBlU2x1ZyBmcm9tICdyZWh5cGUtc2x1ZydcbmltcG9ydCByZWh5cGVBdXRvbGlua0hlYWRpbmdzIGZyb20gJ3JlaHlwZS1hdXRvbGluay1oZWFkaW5ncydcbmltcG9ydCByZWh5cGVTdHJpbmdpZnkgZnJvbSAncmVoeXBlLXN0cmluZ2lmeSdcbmltcG9ydCByZW1hcmtQYXJzZSBmcm9tICdyZW1hcmstcGFyc2UnXG5pbXBvcnQgcmVtYXJrR2ZtIGZyb20gJ3JlbWFyay1nZm0nXG5pbXBvcnQgcmVtYXJrUmVoeXBlIGZyb20gJ3JlbWFyay1yZWh5cGUnXG5pbXBvcnQgcmVtYXJrRnJvbnRtYXR0ZXIgZnJvbSAncmVtYXJrLWZyb250bWF0dGVyJ1xuaW1wb3J0IHsgZ2V0SGlnaGxpZ2h0ZXIsIHR5cGUgSVRoZW1lUmVnaXN0cmF0aW9uLCB0eXBlIExhbmcsIHR5cGUgVGhlbWUgfSBmcm9tICdzaGlraS1lcydcbmltcG9ydCB7IHVuaWZpZWQsIHR5cGUgUHJvY2Vzc29yIH0gZnJvbSAndW5pZmllZCdcblxuY29uc3QgbGFuZ3MgPSBbJ3Z1ZScsICd2dWUtaHRtbCcsICd0eXBlc2NyaXB0JywgJ2Jhc2gnLCAnc2NzcyddIHNhdGlzZmllcyBMYW5nW11cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGNyZWF0ZVByb2Nlc3Nvcih0aGVtZTogSVRoZW1lUmVnaXN0cmF0aW9uKTogUHJvbWlzZTxQcm9jZXNzb3I+IHtcbiAgY29uc3QgaGlnaGxpZ2h0ZXIgPSBhd2FpdCBnZXRIaWdobGlnaHRlcih7XG4gICAgdGhlbWUsXG4gICAgbGFuZ3MsXG4gIH0pXG5cbiAgcmV0dXJuIHVuaWZpZWQoKVxuICAgIC51c2UocmVtYXJrUGFyc2UpXG4gICAgLnVzZShyZW1hcmtGcm9udG1hdHRlcilcbiAgICAudXNlKCgpID0+ICh0cmVlLCBmaWxlKSA9PiB7XG4gICAgICBpZiAodHJlZS5jaGlsZHJlblswXS50eXBlID09PSAneWFtbCcpIHtcbiAgICAgICAgLy8gc3RvcmUgZnJvbnRtYXR0ZXIgaW4gdmZpbGVcbiAgICAgICAgZmlsZS5kYXRhLmZyb250bWF0dGVyID0geWFtbC5sb2FkKHRyZWUuY2hpbGRyZW5bMF0udmFsdWUpXG4gICAgICB9XG4gICAgfSlcbiAgICAudXNlKHJlbWFya0dmbSlcbiAgICAudXNlKHJlbWFya1NoaWtpLCB7IGhpZ2hsaWdodGVyIH0pXG4gICAgLnVzZShyZW1hcmtSZWh5cGUsIHsgYWxsb3dEYW5nZXJvdXNIdG1sOiB0cnVlIH0pXG4gICAgLnVzZShyZWh5cGVSYXcpXG4gICAgLnVzZShyZWh5cGVFeHRlcm5hbExpbmtzLCB7IHJlbDogWydub2ZvbGxvdyddLCB0YXJnZXQ6ICdfYmxhbmsnIH0pXG4gICAgLnVzZShyZWh5cGVTbHVnKVxuICAgIC51c2UocmVoeXBlQXV0b2xpbmtIZWFkaW5ncywge1xuICAgICAgYmVoYXZpb3I6ICdhcHBlbmQnLFxuICAgICAgY29udGVudDoge1xuICAgICAgICB0eXBlOiAnZWxlbWVudCcsXG4gICAgICAgIHRhZ05hbWU6ICdpJyxcbiAgICAgICAgcHJvcGVydGllczoge1xuICAgICAgICAgIGNsYXNzTmFtZTogWydpY29uaWZ5IHRvYy1saW5rLWFuY2hvciddLFxuICAgICAgICAgIGRhdGFJY29uOiAnZmVhdGhlcjpsaW5rJyxcbiAgICAgICAgfSxcbiAgICAgICAgY2hpbGRyZW46IFtdLFxuICAgICAgfSxcbiAgICB9KVxuICAgIC51c2UocmVoeXBlU3RyaW5naWZ5KVxufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gY3JlYXRlUHJvY2Vzc29ycyhcbiAgdGhlbWU6XG4gICAgfCBUaGVtZVxuICAgIHwge1xuICAgICAgICBsaWdodDogVGhlbWVcbiAgICAgICAgZGFyazogVGhlbWVcbiAgICAgIH1cbik6IFByb21pc2U8eyBsaWdodDogUHJvY2Vzc29yOyBkYXJrOiBQcm9jZXNzb3IgfT4ge1xuICByZXR1cm4ge1xuICAgIGxpZ2h0OiBhd2FpdCBjcmVhdGVQcm9jZXNzb3IodHlwZW9mIHRoZW1lID09PSAnc3RyaW5nJyA/IHRoZW1lIDogdGhlbWUubGlnaHQpLFxuICAgIGRhcms6IGF3YWl0IGNyZWF0ZVByb2Nlc3Nvcih0eXBlb2YgdGhlbWUgPT09ICdzdHJpbmcnID8gdGhlbWUgOiB0aGVtZS5kYXJrKSxcbiAgfVxufVxuIiwgImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJEOlxcXFxXZWJzdG9ybVByb2plY3RzXFxcXHRlbXBsYXRlVnVlcm9cXFxcdml0ZS1wbHVnaW4tdnVlcm8tZG9jXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ZpbGVuYW1lID0gXCJEOlxcXFxXZWJzdG9ybVByb2plY3RzXFxcXHRlbXBsYXRlVnVlcm9cXFxcdml0ZS1wbHVnaW4tdnVlcm8tZG9jXFxcXHRyYW5zZm9ybS50c1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9pbXBvcnRfbWV0YV91cmwgPSBcImZpbGU6Ly8vRDovV2Vic3Rvcm1Qcm9qZWN0cy90ZW1wbGF0ZVZ1ZXJvL3ZpdGUtcGx1Z2luLXZ1ZXJvLWRvYy90cmFuc2Zvcm0udHNcIjtpbXBvcnQgeyBrZWJhYkNhc2UgfSBmcm9tICdzY3VsZSdcblxuY29uc3QgU0VMRl9DTE9TSU5HX1RBR19SRUdFWCA9IC88KFteXFxzPjwvXSspKFtePl0rKVxcLz4vZ1xuY29uc3QgT1BFTl9UQUdfUkVHRVggPSAvPChbXlxccz48L10rKS9nXG5jb25zdCBDTE9TRV9UQUdfUkVHRVggPSAvPFxcLyhbXlxccz48L10rKS9nXG5cbmV4cG9ydCBmdW5jdGlvbiB0cmFuc2Zvcm1FeGFtcGxlTWFya3VwKHJhdzogc3RyaW5nKSB7XG4gIGxldCBvdXRwdXQgPSByYXdcbiAgbGV0IGNvbnRlbnQ6IFJlZ0V4cE1hdGNoQXJyYXkgfCBudWxsID0gbnVsbFxuICBpZiAoKGNvbnRlbnQgPSByYXcubWF0Y2goLzwhLS1leGFtcGxlLS0+KFtcXHNcXFNdKj8pPCEtLVxcL2V4YW1wbGUtLT4vKSkpIHtcbiAgICBjb25zdCBrZWJhYkNvbnRlbnQgPSBjb250ZW50WzFdXG4gICAgICAucmVwbGFjZUFsbChTRUxGX0NMT1NJTkdfVEFHX1JFR0VYLCAoc3Vic3RyaW5nLCB0YWcpID0+IHtcbiAgICAgICAgcmV0dXJuIHN1YnN0cmluZy5yZXBsYWNlKCcvPicsIGA+PC8ke3RhZy50cmltKCl9PmApXG4gICAgICB9KVxuICAgICAgLnJlcGxhY2VBbGwoT1BFTl9UQUdfUkVHRVgsIChzdWJzdHJpbmcpID0+IHtcbiAgICAgICAgcmV0dXJuIGA8JHtrZWJhYkNhc2Uoc3Vic3RyaW5nLnN1YnN0cmluZygxKS50cmltKCkpfWBcbiAgICAgIH0pXG4gICAgICAucmVwbGFjZUFsbChDTE9TRV9UQUdfUkVHRVgsIChzdWJzdHJpbmcpID0+IHtcbiAgICAgICAgcmV0dXJuIGA8LyR7a2ViYWJDYXNlKHN1YnN0cmluZy5zdWJzdHJpbmcoMikudHJpbSgpKX1gXG4gICAgICB9KVxuICAgICAgLnJlcGxhY2VBbGwoJyYjeDI3OycsIFwiJ1wiKVxuXG4gICAgb3V0cHV0ID0gb3V0cHV0LnJlcGxhY2UoY29udGVudFsxXSwga2ViYWJDb250ZW50KVxuICB9XG5cbiAgcmV0dXJuIG91dHB1dFxufVxuXG5leHBvcnQgZnVuY3Rpb24gdHJhbnNmb3JtU2xvdHMoc291cmNlOiBzdHJpbmcsIGNvbmRpdGlvbjogc3RyaW5nID0gJycpIHtcbiAgaWYgKHNvdXJjZS5pbmNsdWRlcygnPCEtLWNvZGUtLT4nKSAmJiBzb3VyY2UuaW5jbHVkZXMoJzwhLS1leGFtcGxlLS0+JykpIHtcbiAgICByZXR1cm4gYDx0ZW1wbGF0ZSAke2NvbmRpdGlvbn0gI2RlZmF1bHQ+JHtzb3VyY2V9YFxuICAgICAgLnJlcGxhY2UoXG4gICAgICAgIGA8IS0tY29kZS0tPmAsXG4gICAgICAgIGA8L3RlbXBsYXRlPjx0ZW1wbGF0ZSAke2NvbmRpdGlvbn0gI2NvZGU+XFxuPHNsb3QgbmFtZT1cImNvZGVcIj48ZGl2IHYtcHJlPmBcbiAgICAgIClcbiAgICAgIC5yZXBsYWNlKGA8IS0tL2NvZGUtLT5gLCBgPC9kaXY+PC9zbG90PlxcbjwvdGVtcGxhdGU+YClcbiAgICAgIC5yZXBsYWNlKFxuICAgICAgICBgPCEtLWV4YW1wbGUtLT5gLFxuICAgICAgICBgPHRlbXBsYXRlICR7Y29uZGl0aW9ufSAjZXhhbXBsZT5cXG48c2xvdCBuYW1lPVwiZXhhbXBsZVwiPmBcbiAgICAgIClcbiAgICAgIC5yZXBsYWNlKGA8IS0tL2V4YW1wbGUtLT5gLCBgPC9zbG90PlxcbjwvdGVtcGxhdGU+YClcbiAgfVxuXG4gIGlmIChzb3VyY2UuaW5jbHVkZXMoJzwhLS1jb2RlLS0+JykpIHtcbiAgICByZXR1cm4gYDx0ZW1wbGF0ZSAke2NvbmRpdGlvbn0gI2RlZmF1bHQ+JHtzb3VyY2V9YFxuICAgICAgLnJlcGxhY2UoXG4gICAgICAgIGA8IS0tY29kZS0tPmAsXG4gICAgICAgIGA8L3RlbXBsYXRlPjx0ZW1wbGF0ZSAke2NvbmRpdGlvbn0gI2NvZGU+XFxuPHNsb3QgbmFtZT1cImNvZGVcIj48ZGl2IHYtcHJlPmBcbiAgICAgIClcbiAgICAgIC5yZXBsYWNlKFxuICAgICAgICBgPCEtLS9jb2RlLS0+YCxcbiAgICAgICAgYDwvZGl2Pjwvc2xvdD5cXG48L3RlbXBsYXRlPlxcbjx0ZW1wbGF0ZSAke2NvbmRpdGlvbn0gI2V4YW1wbGU+PHNsb3QgbmFtZT1cImV4YW1wbGVcIj48L3Nsb3Q+PC90ZW1wbGF0ZT5gXG4gICAgICApXG4gIH1cblxuICBpZiAoc291cmNlLmluY2x1ZGVzKCc8IS0tZXhhbXBsZS0tPicpKSB7XG4gICAgcmV0dXJuIGA8dGVtcGxhdGUgJHtjb25kaXRpb259ICNkZWZhdWx0PiR7c291cmNlfWBcbiAgICAgIC5yZXBsYWNlKFxuICAgICAgICBgPCEtLWV4YW1wbGUtLT5gLFxuICAgICAgICBgPC90ZW1wbGF0ZT48dGVtcGxhdGUgJHtjb25kaXRpb259ICNleGFtcGxlPlxcbjxzbG90IG5hbWU9XCJleGFtcGxlXCI+YFxuICAgICAgKVxuICAgICAgLnJlcGxhY2UoXG4gICAgICAgIGA8IS0tL2V4YW1wbGUtLT5gLFxuICAgICAgICBgPC9zbG90PlxcbjwvdGVtcGxhdGU+XFxuPHRlbXBsYXRlICR7Y29uZGl0aW9ufSAjY29kZT48c2xvdCBuYW1lPVwiY29kZVwiPjwvc2xvdD48L3RlbXBsYXRlPmBcbiAgICAgIClcbiAgfVxuXG4gIHJldHVybiAoXG4gICAgYDx0ZW1wbGF0ZSAke2NvbmRpdGlvbn0gI2RlZmF1bHQ+JHtzb3VyY2V9PC90ZW1wbGF0ZT5gICtcbiAgICBgPHRlbXBsYXRlICR7Y29uZGl0aW9ufSAjZXhhbXBsZT48c2xvdCBuYW1lPVwiZXhhbXBsZVwiPjwvc2xvdD48L3RlbXBsYXRlPmAgK1xuICAgIGA8dGVtcGxhdGUgJHtjb25kaXRpb259ICNjb2RlPjxzbG90IG5hbWU9XCJjb2RlXCI+PC9zbG90PjwvdGVtcGxhdGU+YFxuICApXG59XG4iXSwKICAibWFwcGluZ3MiOiAiO0FBQTJSLFNBQVMsU0FBUyxlQUFlO0FBQzVULFNBQVMscUJBQXFCO0FBQzlCLFNBQVMsb0JBQW9CO0FBQzdCLE9BQU8sU0FBUztBQUNoQixPQUFPLGVBQWU7QUFDdEIsU0FBUyw0QkFBNEI7QUFDckMsT0FBTyxnQkFBZ0I7QUFDdkIsT0FBTyxnQkFBZ0I7QUFDdkIsU0FBUyx1QkFBdUI7QUFDaEMsU0FBUyx1QkFBdUI7QUFDaEMsT0FBTyxnQkFBZ0I7QUFDdkIsT0FBTyxjQUFjO0FBQ3JCLE9BQU8sbUJBQW1CO0FBQzFCLFNBQVMsZUFBZTtBQUN4QixPQUFPLGNBQWM7OztBQ1ZyQixTQUFTLE1BQU0sZ0JBQWdCO0FBQy9CLFNBQVMsaUJBQWlCLGFBQWE7OztBQ0xrVCxPQUFPLFVBQVU7QUFDMVcsT0FBTyxpQkFBaUI7QUFDeEIsT0FBTyx5QkFBeUI7QUFDaEMsT0FBTyxlQUFlO0FBQ3RCLE9BQU8sZ0JBQWdCO0FBQ3ZCLE9BQU8sNEJBQTRCO0FBQ25DLE9BQU8scUJBQXFCO0FBQzVCLE9BQU8saUJBQWlCO0FBQ3hCLE9BQU8sZUFBZTtBQUN0QixPQUFPLGtCQUFrQjtBQUN6QixPQUFPLHVCQUF1QjtBQUM5QixTQUFTLHNCQUFzRTtBQUMvRSxTQUFTLGVBQStCO0FBRXhDLElBQU0sUUFBUSxDQUFDLE9BQU8sWUFBWSxjQUFjLFFBQVEsTUFBTTtBQUU5RCxlQUFzQixnQkFBZ0IsT0FBK0M7QUFDbkYsUUFBTSxjQUFjLE1BQU0sZUFBZTtBQUFBLElBQ3ZDO0FBQUEsSUFDQTtBQUFBLEVBQ0YsQ0FBQztBQUVELFNBQU8sUUFBUSxFQUNaLElBQUksV0FBVyxFQUNmLElBQUksaUJBQWlCLEVBQ3JCLElBQUksTUFBTSxDQUFDLE1BQU0sU0FBUztBQUN6QixRQUFJLEtBQUssU0FBUyxHQUFHLFNBQVMsUUFBUTtBQUVwQyxXQUFLLEtBQUssY0FBYyxLQUFLLEtBQUssS0FBSyxTQUFTLEdBQUcsS0FBSztBQUFBLElBQzFEO0FBQUEsRUFDRixDQUFDLEVBQ0EsSUFBSSxTQUFTLEVBQ2IsSUFBSSxhQUFhLEVBQUUsWUFBWSxDQUFDLEVBQ2hDLElBQUksY0FBYyxFQUFFLG9CQUFvQixLQUFLLENBQUMsRUFDOUMsSUFBSSxTQUFTLEVBQ2IsSUFBSSxxQkFBcUIsRUFBRSxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsU0FBUyxDQUFDLEVBQ2hFLElBQUksVUFBVSxFQUNkLElBQUksd0JBQXdCO0FBQUEsSUFDM0IsVUFBVTtBQUFBLElBQ1YsU0FBUztBQUFBLE1BQ1AsTUFBTTtBQUFBLE1BQ04sU0FBUztBQUFBLE1BQ1QsWUFBWTtBQUFBLFFBQ1YsV0FBVyxDQUFDLHlCQUF5QjtBQUFBLFFBQ3JDLFVBQVU7QUFBQSxNQUNaO0FBQUEsTUFDQSxVQUFVLENBQUM7QUFBQSxJQUNiO0FBQUEsRUFDRixDQUFDLEVBQ0EsSUFBSSxlQUFlO0FBQ3hCO0FBRUEsZUFBc0IsaUJBQ3BCLE9BTWdEO0FBQ2hELFNBQU87QUFBQSxJQUNMLE9BQU8sTUFBTSxnQkFBZ0IsT0FBTyxVQUFVLFdBQVcsUUFBUSxNQUFNLEtBQUs7QUFBQSxJQUM1RSxNQUFNLE1BQU0sZ0JBQWdCLE9BQU8sVUFBVSxXQUFXLFFBQVEsTUFBTSxJQUFJO0FBQUEsRUFDNUU7QUFDRjs7O0FDaEUyVixTQUFTLGlCQUFpQjtBQUVyWCxJQUFNLHlCQUF5QjtBQUMvQixJQUFNLGlCQUFpQjtBQUN2QixJQUFNLGtCQUFrQjtBQUVqQixTQUFTLHVCQUF1QixLQUFhO0FBQ2xELE1BQUksU0FBUztBQUNiLE1BQUksVUFBbUM7QUFDdkMsTUFBSyxVQUFVLElBQUksTUFBTSwwQ0FBMEMsR0FBSTtBQUNyRSxVQUFNLGVBQWUsUUFBUSxHQUMxQixXQUFXLHdCQUF3QixDQUFDLFdBQVcsUUFBUTtBQUN0RCxhQUFPLFVBQVUsUUFBUSxNQUFNLE1BQU0sSUFBSSxLQUFLLElBQUk7QUFBQSxJQUNwRCxDQUFDLEVBQ0EsV0FBVyxnQkFBZ0IsQ0FBQyxjQUFjO0FBQ3pDLGFBQU8sSUFBSSxVQUFVLFVBQVUsVUFBVSxDQUFDLEVBQUUsS0FBSyxDQUFDO0FBQUEsSUFDcEQsQ0FBQyxFQUNBLFdBQVcsaUJBQWlCLENBQUMsY0FBYztBQUMxQyxhQUFPLEtBQUssVUFBVSxVQUFVLFVBQVUsQ0FBQyxFQUFFLEtBQUssQ0FBQztBQUFBLElBQ3JELENBQUMsRUFDQSxXQUFXLFVBQVUsR0FBRztBQUUzQixhQUFTLE9BQU8sUUFBUSxRQUFRLElBQUksWUFBWTtBQUFBLEVBQ2xEO0FBRUEsU0FBTztBQUNUO0FBRU8sU0FBUyxlQUFlLFFBQWdCLFlBQW9CLElBQUk7QUFDckUsTUFBSSxPQUFPLFNBQVMsYUFBYSxLQUFLLE9BQU8sU0FBUyxnQkFBZ0IsR0FBRztBQUN2RSxXQUFPLGFBQWEsc0JBQXNCLFNBQ3ZDO0FBQUEsTUFDQztBQUFBLE1BQ0Esd0JBQXdCO0FBQUE7QUFBQSxJQUMxQixFQUNDLFFBQVEsZ0JBQWdCO0FBQUEsWUFBNEIsRUFDcEQ7QUFBQSxNQUNDO0FBQUEsTUFDQSxhQUFhO0FBQUE7QUFBQSxJQUNmLEVBQ0MsUUFBUSxtQkFBbUI7QUFBQSxZQUFzQjtBQUFBLEVBQ3REO0FBRUEsTUFBSSxPQUFPLFNBQVMsYUFBYSxHQUFHO0FBQ2xDLFdBQU8sYUFBYSxzQkFBc0IsU0FDdkM7QUFBQSxNQUNDO0FBQUEsTUFDQSx3QkFBd0I7QUFBQTtBQUFBLElBQzFCLEVBQ0M7QUFBQSxNQUNDO0FBQUEsTUFDQTtBQUFBO0FBQUEsWUFBeUM7QUFBQSxJQUMzQztBQUFBLEVBQ0o7QUFFQSxNQUFJLE9BQU8sU0FBUyxnQkFBZ0IsR0FBRztBQUNyQyxXQUFPLGFBQWEsc0JBQXNCLFNBQ3ZDO0FBQUEsTUFDQztBQUFBLE1BQ0Esd0JBQXdCO0FBQUE7QUFBQSxJQUMxQixFQUNDO0FBQUEsTUFDQztBQUFBLE1BQ0E7QUFBQTtBQUFBLFlBQW1DO0FBQUEsSUFDckM7QUFBQSxFQUNKO0FBRUEsU0FDRSxhQUFhLHNCQUFzQiw4QkFDdEIsdUVBQ0E7QUFFakI7OztBRjlEQSxTQUFTLFFBQVEsSUFBWTtBQUMzQixRQUFNLFFBQVEsR0FBRyxRQUFRLEdBQUc7QUFDNUIsTUFBSSxRQUFRO0FBQUcsV0FBTztBQUFBO0FBQ2pCLFdBQU8sR0FBRyxNQUFNLEdBQUcsS0FBSztBQUMvQjtBQW1CTyxTQUFTLG1CQUFtQixTQUF3QjtBQUN6RCxNQUFJO0FBQ0osTUFBSTtBQUVKLFFBQU0sTUFBTSxRQUFRLElBQUk7QUFDeEIsUUFBTSxhQUFhLFFBQVEsYUFBYSxLQUFLLEtBQUssUUFBUSxVQUFVLElBQUk7QUFFeEUsaUJBQWUsY0FBYyxJQUFZLEtBQWE7QUF4Q3hEO0FBeUNJLFVBQU0sT0FBTyxRQUFRLEVBQUU7QUFJdkIsVUFBTSxRQUFRLHVCQUF1QixHQUFHO0FBR3hDLFFBQUksQ0FBQztBQUFZLG1CQUFhLE1BQU0saUJBQWlCLFFBQVEsTUFBTSxLQUFLO0FBRXhFLFVBQU0sQ0FBQyxZQUFZLFNBQVMsSUFBSSxNQUFNLFFBQVEsSUFBSTtBQUFBLE1BQ2hELFdBQVcsTUFBTSxRQUFRLEtBQUs7QUFBQSxNQUM5QixXQUFXLEtBQUssUUFBUSxLQUFLO0FBQUEsSUFDL0IsQ0FBQztBQUVELFVBQU0sZUFBZSxXQUFXLFNBQVM7QUFDekMsVUFBTSxjQUFjLFVBQVUsU0FBUztBQUN2QyxVQUFNLGdCQUFjLGdCQUFXLFNBQVgsbUJBQWlCLGdCQUFlLENBQUM7QUFHckQsVUFBTSxZQUFZLGVBQWUsY0FBYyx5QkFBeUI7QUFDeEUsVUFBTSxXQUFXLGVBQWUsYUFBYSx3QkFBd0I7QUFHckUsVUFBTSxNQUFNO0FBQUEsTUFDVjtBQUFBLE1BQ0EsTUFBTSxRQUFRO0FBQUEsTUFDZCxPQUFPO0FBQUEsTUFDUCxPQUFPO0FBQUEsTUFDUCxPQUFPLFFBQVE7QUFBQSxNQUNmO0FBQUEsSUFDRixFQUFFLEtBQUssSUFBSTtBQUdYLFVBQU0sU0FBUyxNQUFNLEtBQUs7QUFBQSxNQUN4QixVQUFVO0FBQUEsTUFDVixXQUFXO0FBQUEsSUFDYixDQUFDO0FBRUQsUUFBSSxPQUFPLE9BQU8sVUFBVSxPQUFPLFdBQVcsYUFBYSxNQUFNO0FBQy9ELGNBQVEsTUFBTSxPQUFPLE1BQU07QUFFM0IsWUFBTSxJQUFJLE1BQU0seURBQXlELEtBQUs7QUFBQSxJQUNoRjtBQUdBLFVBQU0sRUFBRSxNQUFNLE9BQU8sSUFBSSxnQkFBZ0I7QUFBQSxNQUN2QyxVQUFVO0FBQUEsTUFDVixJQUFJO0FBQUEsTUFDSixRQUFRLE9BQU8sV0FBVyxTQUFTO0FBQUEsTUFDbkMsZ0JBQWdCLE9BQU8sV0FBVyxTQUFTO0FBQUEsTUFDM0Msb0JBQW9CO0FBQUEsSUFDdEIsQ0FBQztBQUVELFFBQUksT0FBTyxRQUFRO0FBQ2pCLGNBQVEsTUFBTSxNQUFNO0FBRXBCLFlBQU0sSUFBSSxNQUFNLDZDQUE2QyxLQUFLO0FBQUEsSUFDcEU7QUFHQSxRQUFJLGFBQWE7QUFDakIsU0FBSSxhQUFRLGVBQVIsbUJBQW9CLFNBQVM7QUFDL0IsbUJBQWEsS0FBSyxVQUFVO0FBQUEsUUFDMUIsY0FBYyxLQUFLLFVBQVUsSUFBSSxNQUFNO0FBQUEsUUFDdkMsVUFBVSxTQUFTLElBQUk7QUFBQSxRQUN2QixPQUFNLGlDQUFRLGdCQUFlLEtBQUs7QUFBQSxRQUNsQyxlQUFjLGlDQUFRLGdCQUFlLEtBQUssUUFBUSxXQUFXO0FBQUEsTUFDL0QsQ0FBQztBQUFBLElBQ0g7QUFHQSxVQUFNLFNBQVM7QUFBQSxNQUNiO0FBQUEsTUFDQTtBQUFBLE1BRUEsS0FBSyxRQUFRLDBCQUEwQixpQkFBaUI7QUFBQSxNQUV4RCx5QkFBeUIsS0FBSyxVQUFVLFdBQVc7QUFBQSxNQUNuRDtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQSxpQkFBaUI7QUFBQSxNQUNqQjtBQUFBLE1BQ0E7QUFBQSxPQUVBLGlDQUFRLGdCQUFlLEtBQUssc0JBQXNCLEtBQUssVUFBVSxJQUFJO0FBQUEsTUFDckU7QUFBQSxJQUNGLEVBQUUsS0FBSyxJQUFJO0FBRVgsV0FBTztBQUFBLEVBQ1Q7QUFFQSxTQUFPO0FBQUEsSUFDTCxNQUFNO0FBQUEsSUFDTixTQUFTO0FBQUEsSUFDVCxlQUFlLFNBQVM7QUFDdEIsZUFBUztBQUFBLElBQ1g7QUFBQSxJQUNBLE1BQU0sVUFBVSxLQUFLLElBQUk7QUFDdkIsVUFBSSxHQUFHLFNBQVMsS0FBSyxLQUFLLEdBQUcsV0FBVyxVQUFVLEdBQUc7QUFDbkQsZUFBTyxNQUFNLGNBQWMsSUFBSSxHQUFHO0FBQUEsTUFDcEM7QUFBQSxJQUNGO0FBQUEsRUFDRjtBQUNGOzs7QURqSitLLElBQU0sMkNBQTJDO0FBb0JoTyxJQUFNLGdCQUFnQixRQUFRLElBQUksU0FBUyxRQUFRLElBQUksV0FBVyxTQUFTO0FBTzNFLElBQU8sc0JBQVEsYUFBYTtBQUFBLEVBRTFCLE1BQU0sUUFBUSxJQUFJO0FBQUEsRUFLbEIsTUFBTTtBQUFBLEVBRU4sV0FBVztBQUFBLEVBRVgsVUFBVTtBQUFBLEVBRVYsUUFBUTtBQUFBLElBRU4sTUFBTTtBQUFBLEVBQ1I7QUFBQSxFQVFBLGNBQWM7QUFBQSxJQUNaLFNBQVM7QUFBQSxNQUNQO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0E7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLElBQ0Y7QUFBQSxJQUNBLFVBQVU7QUFBQSxFQUNaO0FBQUEsRUFFQSxTQUFTO0FBQUEsSUFDUCxPQUFPO0FBQUEsTUFDTDtBQUFBLFFBQ0UsTUFBTTtBQUFBLFFBQ04sYUFBYTtBQUFBLE1BQ2Y7QUFBQSxJQUNGO0FBQUEsRUFDRjtBQUFBLEVBQ0EsT0FBTztBQUFBLElBQ0wsUUFBUTtBQUFBLElBS1IsbUJBQW1CLE9BQU87QUFBQSxJQUMxQixpQkFBaUIsRUFBRSxTQUFTLENBQUMsRUFBRTtBQUFBLEVBQ2pDO0FBQUEsRUFDQSxTQUFTO0FBQUEsSUFNUCxJQUFJO0FBQUEsTUFDRixTQUFTLENBQUMsUUFBUTtBQUFBLElBQ3BCLENBQUM7QUFBQSxJQU9ELGNBQWM7QUFBQSxNQUNaLFNBQVMsUUFBUSxRQUFRLGNBQWMsd0NBQWUsQ0FBQyxHQUFHLGtCQUFrQjtBQUFBLE1BQzVFLGFBQWE7QUFBQSxNQUNiLGlCQUFpQjtBQUFBLElBQ25CLENBQUM7QUFBQSxJQVNELFVBQVU7QUFBQSxNQUNSLGNBQWM7QUFBQSxNQVFkLGNBQWM7QUFBQSxJQUNoQixDQUFDO0FBQUEsSUFPRCxXQUFXO0FBQUEsTUFDVCxLQUFLO0FBQUEsTUFDTCxTQUFTLENBQUMsT0FBTyxnQkFBZ0Isb0JBQW9CO0FBQUEsSUFDdkQsQ0FBQztBQUFBLElBVUQsbUJBQW1CO0FBQUEsTUFDakIsWUFBWTtBQUFBLE1BQ1osa0JBQWtCO0FBQUEsTUFDbEIsT0FBTztBQUFBLFFBQ0wsT0FBTztBQUFBLFVBQ0wsT0FBTztBQUFBLFVBQ1AsTUFBTTtBQUFBLFFBQ1I7QUFBQSxNQUNGO0FBQUEsTUFDQSxZQUFZO0FBQUEsUUFDVixTQUFTO0FBQUEsUUFDVCxjQUFjO0FBQUEsTUFDaEI7QUFBQSxJQUNGLENBQUM7QUFBQSxJQVFELFdBQVc7QUFBQSxNQUNULE1BQU0sQ0FBQyxpQkFBaUIsa0JBQWtCLGFBQWE7QUFBQSxNQUN2RCxZQUFZLENBQUMsT0FBTyxJQUFJO0FBQUEsTUFDeEIsS0FBSztBQUFBLE1BQ0wsU0FBUyxDQUFDLFVBQVUsY0FBYyxPQUFPO0FBQUEsSUFDM0MsQ0FBQztBQUFBLElBUUQsV0FBVztBQUFBLElBT1gsZ0JBQWdCO0FBQUEsTUFDZCxRQUFRO0FBQUEsUUFDTixVQUFVO0FBQUEsVUFDUjtBQUFBLFlBQ0UsTUFBTTtBQUFBLFlBQ04sUUFBUTtBQUFBLFVBQ1Y7QUFBQSxVQUNBO0FBQUEsWUFDRSxNQUFNO0FBQUEsWUFDTixRQUFRO0FBQUEsVUFDVjtBQUFBLFVBQ0E7QUFBQSxZQUNFLE1BQU07QUFBQSxZQUNOLFFBQVE7QUFBQSxVQUNWO0FBQUEsUUFDRjtBQUFBLE1BQ0Y7QUFBQSxJQUNGLENBQUM7QUFBQSxJQU9ELENBQUMsUUFBUSxJQUFJLFNBQ1QsU0FDQSxnQkFBZ0I7QUFBQSxNQUNkLEtBQUs7QUFBQSxRQUNILElBQUksUUFBUSxJQUFJO0FBQUEsTUFDbEI7QUFBQSxJQUNGLENBQUM7QUFBQSxJQU9MLFFBQVE7QUFBQSxNQUNOLE1BQU07QUFBQSxNQUNOLGVBQWUsQ0FBQyxlQUFlLGVBQWUsY0FBYyxzQkFBc0I7QUFBQSxNQUNsRixVQUFVO0FBQUEsUUFDUixNQUFNO0FBQUEsUUFDTixZQUFZO0FBQUEsUUFDWixXQUFXO0FBQUEsUUFDWCxTQUFTO0FBQUEsUUFDVCxhQUFhO0FBQUEsUUFDYixrQkFBa0I7QUFBQSxRQUNsQixPQUFPO0FBQUEsVUFDTDtBQUFBLFlBQ0UsS0FBSztBQUFBLFlBQ0wsT0FBTztBQUFBLFlBQ1AsTUFBTTtBQUFBLFVBQ1I7QUFBQSxVQUNBO0FBQUEsWUFDRSxLQUFLO0FBQUEsWUFDTCxPQUFPO0FBQUEsWUFDUCxNQUFNO0FBQUEsVUFDUjtBQUFBLFVBQ0E7QUFBQSxZQUNFLEtBQUs7QUFBQSxZQUNMLE9BQU87QUFBQSxZQUNQLE1BQU07QUFBQSxZQUNOLFNBQVM7QUFBQSxVQUNYO0FBQUEsUUFDRjtBQUFBLE1BQ0Y7QUFBQSxJQUNGLENBQUM7QUFBQSxJQVFELFNBQVM7QUFBQSxNQUNQLFFBQVE7QUFBQSxNQUNSLFNBQVMsQ0FBQyxnQkFBZ0I7QUFBQSxNQUMxQixXQUFXO0FBQUEsTUFDWCxVQUFVO0FBQUEsUUFDUixVQUFVO0FBQUEsVUFDUjtBQUFBLFVBQ0E7QUFBQSxVQUNBO0FBQUEsVUFDQTtBQUFBLFVBQ0E7QUFBQSxRQUNGO0FBQUEsTUFDRjtBQUFBLE1BQ0EsaUJBQWlCLFNBQVM7QUFDeEIsY0FBTSw0QkFBNEIsUUFBUSxRQUFRLDBCQUEwQixFQUFFO0FBQzlFLGVBQU8sMEJBQTBCLE1BQU0sa0NBQWtDLEtBQUssQ0FBQztBQUFBLE1BQ2pGO0FBQUEsSUFDRixDQUFDO0FBQUEsSUFPRCxDQUFDLGdCQUNHLFNBQ0EsU0FBUztBQUFBLE1BQ1AsVUFBVTtBQUFBLFFBQ1IsbUJBQW1CO0FBQUEsUUFDbkIsWUFBWTtBQUFBLE1BQ2Q7QUFBQSxNQUNBLFNBQVM7QUFBQSxRQUNQLG1CQUFtQjtBQUFBLE1BQ3JCO0FBQUEsTUFDQSxTQUFTO0FBQUEsUUFDUCxTQUFTO0FBQUEsTUFDWDtBQUFBLE1BQ0EsVUFBVTtBQUFBLFFBQ1IsU0FBUyxDQUFDLEtBQUssR0FBRztBQUFBLFFBQ2xCLE9BQU87QUFBQSxNQUNUO0FBQUEsTUFDQSxNQUFNO0FBQUEsUUFDSixTQUFTO0FBQUEsVUFDUDtBQUFBLFlBQ0UsTUFBTTtBQUFBLFlBQ04sUUFBUTtBQUFBLFVBQ1Y7QUFBQSxVQUNBO0FBQUEsWUFDRSxNQUFNO0FBQUEsWUFDTixRQUFRO0FBQUEsVUFDVjtBQUFBLFFBQ0Y7QUFBQSxNQUNGO0FBQUEsSUFDRixDQUFDO0FBQUEsRUFDUDtBQUNGLENBQUM7IiwKICAibmFtZXMiOiBbXQp9Cg==
